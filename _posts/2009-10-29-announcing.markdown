---
title: Announcing the Asgard Project
layout: post
tags: announcement
author: Myles Braithwaite
---

Today I would like for formally announce the **Asgard Project** a
collection of [Open Source][1] [Django][2] applications for running a
personal website (or corporate). We are currently in the [alpha][3]
stage of development and plan to have a release candidate ready for
January 2010.

If you are interested in contributing please visit out
[Gitorious Team][4].

[1]: http://en.wikipedia.org/wiki/Open_Source "Open Source"
[2]: http://djangoproject.org/ "Django Project"
[3]: http://en.wikipedia.org/wiki/Development_stage#Alpha "Development Stage Alpha"
[4]: http://gitorious.org/+asgard-project "Asgard Project Gitorious Team"