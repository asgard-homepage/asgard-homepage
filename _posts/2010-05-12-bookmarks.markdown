---
title: Asgard Bookmarks 0.2.0
layout: post
tags: announcement
author: Myles Braithwaite
---

We have released version 0.2.0 of **Asgard**'s [bookmarks][bookmarks]
application.

* Download ([HTTP][http] & [FTP][ftp])
* [Commits][commits]

If you are interesting in contributing feel free to fork the 
[GitHub Project][project].

### Changes

* Removed the Haystack support.
* Switched from **Django Tagging** to **Django Taggit**.

[bookmarks]: /apps/bookmarks.html
[http]: http://ftp.mylesbraithwaite.com/pub/asgard/bookmarks/asgard-bookmarks-0.2.0.tar.gz
[ftp]: ftp://ftp.mylesbraithwaite.com/pub/asgard/bookmarks/asgard-bookmarks-0.2.0.tar.gz
[project]: http://github.com/asgardproject/asgard-bookmarks
[commits]: http://github.com/asgardproject/asgard-bookmarks/tree/v0.2.0