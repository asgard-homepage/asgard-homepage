---
layout: app
title: Blog
---

A simple [Blogging][1] application for [Django][2].

* [Repository][3]
* [Documentation][4]
* [Ticket Tracker][5]
* Downloads: ([HTTP][6] or [FTP][7])

### Features

* [Categories](http://en.wikipedia.org/wiki/Taxonomy "Wikipedia Article on Taxonomy")
* [Tags](http://en.wikipedia.org/wiki/Tag_%28metadata%29 "Wikipedia Article on Tag (metadata)")
* [hAtom](http://microformats.org/wiki/hatom)
* Basic Search
* Django Sitemaps & Feeds

### Requirements

* [Django Taggit][8] version 3 or higher
* [Asgard Utilities][9]

* *Optional*: [BeautifulSoup][10]

[1]: http://en.wikipedia.org/wiki/Blog "Wikipedia Article on Blog"
[2]: http://djangoproject.org "Django Project"
[3]: http://github.com/asgardproject/asgard-blog "Asgard's Blog Repository on GitHub"
[4]: ../docs/blog/index.html "Asgard Blog Documentation"
[5]: http://myles.lighthouseapp.com/projects/44962-asgard-blog "Asgard's Blog Ticket Tracker on Lighthouse"
[6]: http://ftp.mylesbraithwaite.com/pub/asgard/blog "Download Asgard Blog using HTTP"
[7]: ftp://ftp.mylesbraithwaite.com/pub/asgard/blog "Download Asgard Blog using FTP"
[8]: http://github.com/alex/django-taggit
[9]: ../utils.html
[10]: http://www.crummy.com/software/BeautifulSoup/