---
layout: app
title: Bookmarks
---

A simple Bookmarking application for [Django][django].

* [Repository](http://github.com/asgardproject/asgard-bookmarks/)
* [Documentation](../docs/bookmarks/index.html)
* [Ticket Tracker](http://myles.lighthouseapp.com/projects/44963-asgard-bookmarks)
* Downloads: ([HTTP](http://ftp.mylesbraithwaite.com/pub/asgard/bookmarks/) or [FTP](ftp://ftp.mylesbraithwaite.com/pub/asgard/bookmarks/))

### Features

* [Tags](http://en.wikipedia.org/wiki/Tag_%28metadata%29 "Wikipedia Article on Tag (metadata)")
* Basic Search
* Django Sitemaps & Feeds

### Requirements

* [Django Taggit][django-taggit] version 3 or higher
* [Asgard Utilities][asgard-utils]

[django]: http://djangoproject.com/
[django-taggit]: http://github.com/alex/django-taggit
[asgard-utils]: ../utils.html