#!/bin/bash

rm -fr ../_tmp/apps/repos
rm -fr ../_site/docs

mkdir -p ../_tmp/apps/repos
mkdir -p ../_site/docs

cd ../_tmp/apps/repos

PROJECTS=( 'asgard-blog' )

for PROJECT in "${PROJECTS[@]}"
do
	echo "cloning $PROJECT"
	git clone -q git://gitorious.org/$PROJECT/$PROJECT.git >> /dev/null
	cd $PROJECT/docs/
	echo "making the documentation for $PROJECT"
	make html >> /dev/null
	cd ../../../../../_site/docs/
	echo "copying the documentation for $PROJECT"
	SHORTNAME=${PROJECT#*-}
	cp -r ../../_tmp/apps/repos/$PROJECT/docs/_build/html $SHORTNAME
	cd ../../_tasks
done